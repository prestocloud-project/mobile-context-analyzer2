# MCA Adapters

This directory contains adapters

Overview
-----------
Two adapters are provided :
a) MCA - DIAFDRercom Adapter
b) MCA - Monitoring Adapter

The MCA-DIAFDRecom Adapter supports the event-driven communication between the MCA and the PrEstoCloud DIAFDRecom 
The MCA-Monitring Adapters suports the event-driven communication between the MCA and the PrEstoCloud Monitoring Agents

Installation
-------------
Install python 2.7 and the corresponding libraries from the file requirements.txt

Configuration
----------
Edit config.properties as in the following example :

```
[Broker]
rmqhost=1.2.3.4 #the IP address of PrEstoCloud Broker (RabbitMQ)
rmquser=youruser   #Broker username
rmqpass=yourpass   #Broker password
exchange=presto.cloud #Broker exchange name

[DIAFDRecom]
request_topic=topology_adaptation.excluded_devices.req # DIAFDRecom request topic
reply_topic=topology_adaptation.excluded_devices.ack   # DIAFDRecom reply   topic

[MCA]
metric=task_time        #metric to use for edge device selection
exclude_percent=20      #percent of devices to exclude
mcahost=127.0.0.1       #the hostname or IP of MCA server
mcauser=presto@mycompany.com                #MCA username
mcapass=mypassword                #MCA password

[Monitoring]
topic=monitoring.edgesimulation.#     #the topics(s) to subscribe for monitoring events
```

Execution
---------------
a) Execute the MCA-DIAFDRecom adapter

```
nohup python MCA_DIAFDRecom_Adapter.py
```

b) Execute the MCA-Monitoring adapter

```
nohup python MCA_Monitoring_Adapter.py
```



