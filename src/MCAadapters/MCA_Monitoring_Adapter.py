#!/usr/bin/env python
import pika
import ConfigParser
import requests
import datetime
import json
import os
import urllib

config = ConfigParser.RawConfigParser()
config.read('config.properties')

#Broker props
rmqhost  = config.get('Broker', 'rmqhost');
rmquser  = config.get('Broker', 'rmquser');
rmqpass  = config.get('Broker', 'rmqpass');
exchange = config.get('Broker', 'exchange');

#Monitoring
monitoring_topic = config.get('Monitoring', 'topic');

#MCA props
mcahost = config.get('MCA', 'mcahost');
mcauser = config.get('MCA', 'mcauser');
mcapass = config.get('MCA', 'mcapass');

credentials = pika.PlainCredentials(rmquser ,rmqpass)
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host=rmqhost,credentials=credentials))

channel = connection.channel()

channel.exchange_declare(
        exchange=exchange,
        exchange_type='topic',
        durable=True
        )

result = channel.queue_declare('',exclusive=True)
queue_name = result.method.queue

print "queue_name :",queue_name

channel.queue_bind(
        exchange=exchange,
        queue=queue_name, 
        routing_key=monitoring_topic)

print(' [*] MCA-Monitoring Adapter') 
print(' [*] Waiting for events. To exit press CTRL+C')

def sendToMCA(mcahost, metric_name,device_id,fragment_id,value):
   #(metric_name,device_id,fragment_id,value
   print mcauser
   #fragment_id = urllib.quote_plus(fragment_id)
   device_id = device_id.replace(":","_")
   print device_id
   #value = urllib.quote_plus(str(value))
   uri =  'http://%s/MCAMANAGER/services/call/json/putObsMetricValue/%s/%s/%s/%s' % ( mcahost, metric_name,device_id,fragment_id,value)
   print uri
   response = requests.get( uri, auth=(mcauser,mcapass))
   if response:
      print('MCA request completed!')
   else:
      print('MCA request failed')
      print response


def callback(ch, method, properties, body):
   print ">>>> " ,datetime.datetime.now()
   #print("Received [x] %r:%r" % (method.routing_key, body))
   print("Received [x] %r" % (method.routing_key))
   #print body
   event = json.loads(body)
   #print(json.dumps(event, indent=4, sort_keys=True))
   print 'event["fragid"],event["response_time_ms"],event["latency_ms"],event["res_inst"]'
   print event["fragid"],event["response_time_ms"],event["latency_ms"],event["res_inst"]

   device_id = event["res_inst"]
   fragment_id = event["fragid"]

   metric_name = "latency_ms"
   value = event["latency_ms"]
   sendToMCA(mcahost, metric_name,device_id,fragment_id,value)
   metric_name = "response_time_ms"
   value = event["response_time_ms"]
   sendToMCA(mcahost, metric_name,device_id,fragment_id,value)

channel.basic_consume(queue_name, callback, auto_ack=True)
channel.start_consuming()

