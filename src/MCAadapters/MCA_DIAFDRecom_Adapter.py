#!/usr/bin/env python
import pika
import ConfigParser
import requests
import datetime
import json
import os


config = ConfigParser.RawConfigParser()
config.read('config.properties')

#Broker props
rmqhost  = config.get('Broker', 'rmqhost');
rmquser  = config.get('Broker', 'rmquser');
rmqpass  = config.get('Broker', 'rmqpass');
exchange = config.get('Broker', 'exchange');
request_topic = config.get('DIAFDRecom', 'request_topic');
reply_topic = config.get('DIAFDRecom', 'reply_topic');

#MCA props
metric  = config.get('MCA', 'metric');
pct     = config.get('MCA', 'exclude_percent');
mcahost = config.get('MCA', 'mcahost');
mcauser = config.get('MCA', 'mcauser');
mcapass = config.get('MCA', 'mcapass');

credentials = pika.PlainCredentials(rmquser ,rmqpass)
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host=rmqhost,credentials=credentials))

channel = connection.channel()

channel.exchange_declare(
        exchange=exchange,
        exchange_type='topic',
        durable=True
        )

result = channel.queue_declare('',exclusive=True)
queue_name = result.method.queue

print "queue_name :",queue_name

channel.queue_bind(
        exchange=exchange,
        queue=queue_name, 
        routing_key=request_topic)

print(' [*] MCA-DIAFDRecom Adapter') 
print(' [*] Waiting for commands. To exit press CTRL+C')

def callback(ch, method, properties, body):
   print ">>>> " ,datetime.datetime.now()
   print("Received [x] %r:%r" % (method.routing_key, body))
   response = requests.get(
           'http://%s/MCAMANAGER/services/call/json/getExcludedDevicesPerMetric/%s/%s' % ( mcahost, metric, pct), 
           auth=(mcauser,mcapass))
   if response:
      print('MCA request completed!')
      payload = response.json()["data"]
      r_message = json.dumps(payload,indent=4, sort_keys=True)
      #print response.text
      channel.basic_publish(
       exchange=exchange, routing_key=reply_topic, 
       body=r_message)
   else:
      print('MCA request failed')

channel.basic_consume(queue_name, callback, auto_ack=True)
channel.start_consuming()

