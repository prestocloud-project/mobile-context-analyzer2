#!/usr/bin/env python
import pika
import sys
import ConfigParser
import json
import random
import time

M = 16**4 #a constant

config = ConfigParser.RawConfigParser()
config.read('config.properties')

N_DEVICES = 6 # number of devices
N_FRAGS   = 6 # number of fragments
MAX_OBS   = 100 # maximum obs to generate
N_DEVICES = int( config.get('EdgeMonitoringSimulator', 'devices')  );
N_FRAGS   = int( config.get('EdgeMonitoringSimulator', 'fragments'));
MAX_OBS   = int( config.get('EdgeMonitoringSimulator', 'maxobs')   ) ;
SLEEP     = 1

#Broker props
rmqhost  = config.get('Broker', 'rmqhost');
rmquser  = config.get('Broker', 'rmquser');
rmqpass  = config.get('Broker', 'rmqpass');
exchange = config.get('Broker', 'exchange');

topic    = config.get('EdgeMonitoringSimulator', 'events_topic');

devices = []
fragments = []

for n in range(N_DEVICES):
    rand_ipv6 = "2001:" + ":".join(("%x" % random.randint(0, M) for i in range(7)))
    devices.append( rand_ipv6 )

print devices

for f in range(N_FRAGS):
    fragments.append( "FragmentID" + str(f))
print fragments

with open('template.json', 'r') as handle:
    template = json.load(handle)




credentials = pika.PlainCredentials(rmquser ,rmqpass)
connection  = pika.BlockingConnection(
        pika.ConnectionParameters(host=rmqhost,credentials=credentials)
        )

channel = connection.channel()

channel.exchange_declare(
        exchange=exchange,
        exchange_type='topic',
        durable=True
        )

result = channel.queue_declare('',exclusive=True)
queue_name = result.method.queue

print "queue_name :",queue_name

channel.queue_bind (
        exchange=exchange,
        queue=queue_name,
        routing_key=topic
        )
routing_key = topic

message = ""

i=0
while i < MAX_OBS:
    #print(json.dumps(parsed, indent=4, sort_keys=True))
    template["response_time_ms"]=random.uniform(1.0,10.0)
    template["latency_ms"]=random.uniform(1.0,10.0)
    template["res_inst"] = devices[random.randrange(N_DEVICES)]
    fragid = fragments[random.randrange(N_FRAGS)]
    template["fragid"] = fragid
    template["loadBalancerMonitoringModels"][0]["loadbalancer_for_fragid"] = fragid
    print(json.dumps(template, indent=4, sort_keys=True))
    message = json.dumps(template, indent=4, sort_keys=True)
    i+=1
    channel.basic_publish(
       exchange=exchange, routing_key=routing_key, body=message)
    print(" [x] Sent %r:%r" % (routing_key, message))
    time.sleep(SLEEP)

connection.close()

