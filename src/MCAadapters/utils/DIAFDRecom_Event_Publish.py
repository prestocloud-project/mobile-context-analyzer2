#!/usr/bin/env python
import pika
import sys
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('config.properties')


#Broker props
rmqhost  = config.get('Broker', 'rmqhost');
rmquser  = config.get('Broker', 'rmquser');
rmqpass  = config.get('Broker', 'rmqpass');
exchange = config.get('Broker', 'exchange');
topic    = config.get('DIAFDRecom', 'request_topic');
#MCA props
metric   = config.get('MCA'   , 'metric');

credentials = pika.PlainCredentials(rmquser ,rmqpass)
connection  = pika.BlockingConnection(
        pika.ConnectionParameters(host=rmqhost,credentials=credentials)
        )

channel = connection.channel()

channel.exchange_declare(
        exchange=exchange,
        exchange_type='topic',
        durable=True
        )

result = channel.queue_declare('',exclusive=True)
queue_name = result.method.queue

print "queue_name :",queue_name

channel.queue_bind (
        exchange=exchange,
        queue=queue_name,
        routing_key=topic
        )
routing_key = topic

message = ' [ "requesting_excluded_devices" ] '

channel.basic_publish(
    exchange=exchange, routing_key=routing_key, body=message)
print(" [x] Sent %r:%r" % (routing_key, message))

connection.close()
