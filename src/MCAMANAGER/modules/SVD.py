#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import random
import pandas as pd
import seaborn as sns

from surprise import *
from surprise import accuracy
from surprise.model_selection import train_test_split

from surprise.model_selection import cross_validate
from surprise.model_selection import GridSearchCV

from IPython.display import display_html
from IPython.core.display import HTML

np.set_printoptions(precision=2) #print with 2 decimals
pd.options.mode.use_inf_as_na = True
sns.set()

def run_svd(d1):
    #retrieve task_times
    #N_FACTORS
    
    N_DEV = task_times.shape[0] # number of devices 
    N_FR = task_times.shape[1] # number of fragments
    #Convert to suprise input format
    d1 = [] #train    
    for i in range(0,N_DEV):
        for j in range(0,N_FR):
            device_type = i
            task_type = j
            #print i,j,task_times[i,j]
            r = task_times[device_type,task_type] 
            if r > 0: #Ommit negative ratings 
                data = (device_type, task_type, r, device_type)
                d1.append(data)    
                
    train_df = pd.DataFrame(d1, columns=('DeviceID', 'Fragment', 'ExecutionTime', 'DeviceType')) 
    #print train_df
    max_time = train_df['ExecutionTime'].max()
    min_time = train_df['ExecutionTime'].min()
    rd = Reader(rating_scale=(min_time, max_time))
    train_data = Dataset.load_from_df(train_df[['DeviceID', 'Fragment', 'ExecutionTime']], rd)
    #print train_data
    trainset = train_data.build_full_trainset()    
    
    #Search automatically for best SVD params
    
    #https://kerpanic.wordpress.com/2018/03/26/a-gentle-guide-to-recommender-systems-with-surprise/
    param_grid = {'n_factors': [2,3,4,5,6,7,110, 120, 140, 160], 'n_epochs': [20, 50, 100, 150], 
              'lr_all': [0.001, 0.003, 0.005, 0.008],
              'reg_all': [0.08, 0.02, 0.1, 0.15]}
    gs = GridSearchCV(SVD, param_grid, measures=['rmse', 'mae'], cv=3, n_jobs=-1) # -1: all CPUs used.
    # RandomizedSearchCV alternative : As opposed to GridSearchCV, which uses an exhaustive combinatorial 
    # approach, RandomizedSearchCV samples randomly from the parameter space. 
    gs.fit(train_data)
    print "choosing best_estimator "
    print "Best params:", gs.best_params #['rmse']
    print "Best score:", gs.best_score #['rmse']    
    #print "Algo rmse best factors =", gs.best_params['rmse']['n_factors']
    algo = None
    n_factors = None
    #Choose as best estimator the one with the best score !!!
    if gs.best_score['rmse'] > gs.best_score['mae']:
        algo = gs.best_estimator['rmse']
        n_factors = gs.best_params['rmse']['n_factors']
        print "best estimator is rmse"
    else:
        algo = gs.best_estimator['mae']
        n_factors = gs.best_params['mae']['n_factors']
        print "best estimator is mae"
        
    print "algo=", algo        
    
    #cross_validate(algo, train_data, measures=['RMSE', 'MAE'], cv=5, verbose=True)
    
    print "Running algo with best params"
    algo.fit(trainset)
    train_pred = algo.test(trainset.build_testset())
    rmse = accuracy.rmse(train_pred)
    mae = accuracy.mae(train_pred)
    print "Accuracy rmse=",rmse," mae=",mae
    #######################
    '''
    
    print "Running algo with n_factors = ", N_FACTORS
    #algo = SVD(n_factors=N_FACTORS,n_epochs=20,reg_all=0.002) #reg_all – The regularization term. Default is 0.02
    #algo = SVD(n_factors=N_FACTORS,biased=False) #n_factos must be lower than number of devices or fragments (default =100, overfits SCD if big)
    #algo = SVD(verbose=True)
    #algo = KNNBaseline()
    algo = SVD(n_factors=N_FACTORS)
    algo.fit(trainset)
    train_pred = algo.test(trainset.build_testset())
    accuracy.rmse(train_pred)
    accuracy.mae(train_pred)            
    '''
    
    #cross_validate(algo, train_data, measures=['RMSE', 'MAE'], cv=5, verbose=True)
    #cr = cross_validate(algo, train_data, measures=['RMSE', 'MAE'], cv=5, verbose=True)
    #c = []
    ret_arr = np.zeros(task_times.shape)
    for i in range(0,N_DEV):
        for j in range(0,N_FR):            
            #print i,j,task_times[i,j]
            r = task_times[device_type,task_type]
            result = algo.predict(i,j,verbose=False)        
            #print index,result
            est = result.est
            ret_arr[i,j] = est    
    #out_df = pd.DataFrame(c, columns=('DeviceID','DeviceType', 'Fragment', 'Real','Predicted','Error')
    return ret_arr, rmse, n_factors # predicted values array , root mean square error
