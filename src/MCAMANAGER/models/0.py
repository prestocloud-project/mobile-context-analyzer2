from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'MCA'
settings.subtitle = 'PrEstoCloud'
settings.author = 'Nikos Papageorgiou'
settings.author_email = 'npapag@mail.ntua.gr'
settings.keywords = 'context, analyzer'
settings.description = 'Mobile Context Analyzer'
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '1ccc57a0-53ba-4b6a-ab37-ec808a25e6a2'
settings.email_server = 'logging'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
