# -*- coding: utf-8 -*-
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import random
import time

#from scipy import stats
import pandas as pd
from surprise import *
from surprise import accuracy
from surprise.model_selection import train_test_split
from surprise.model_selection import cross_validate
from surprise.model_selection import GridSearchCV
np.set_printoptions(precision=2) #print with 2 decimals
pd.options.mode.use_inf_as_na = True

def get_metric_max(metric_name):
    tbl = db.t_observed_metric
    val_ = -1 
    q = (tbl.f_name==metric_name)     
    #for r in  db(q).select(tbl.f_value.max):     
    #   val_ = r
    val_ = db(q).select(tbl.f_name, groupby=tbl.f_name, having=q)
    return val_

def get_metric_value(metric_name,device,fragment,tbl):
    #tbl = db.t_observed_metric
    val = -1 
    q = (tbl.f_name==metric_name) & (tbl.f_fragment==fragment) & (tbl.f_device==device)    
    for r in db(q).select(tbl.f_value):
        val = r.f_value
    return val

def get_metric_data_arr(metric_name,tbl,max_devices=5,max_fragments=4): 
    ### return an array with values or -1 when not exists in DB    
    ### used in heatmap visualization
    devices = []
    for r in db(tbl.f_name == metric_name).select(tbl.f_device, distinct=True, orderby=tbl.f_device):
        devices.append(r.f_device)
    n_devices = len(devices)    
    if (n_devices) > max_devices:
        n_devices = max_devices
    fragments = []
    for r in db(tbl.f_name == metric_name).select(tbl.f_fragment, distinct=True, orderby=tbl.f_fragment):
        fragments.append(r.f_fragment)
    n_fragments = len(fragments)
    if (n_fragments) > max_fragments:
        n_fragments = max_fragments
    ###
    values = np.full((n_devices,n_fragments),-1.0)    
    ###
    max_v = -1.0
    d = 0    
    for dev in devices:
        f = 0
        for frg in fragments:            
            #values[d,f] = get_metric_value(metric_name,d,f,tbl)
            values[d,f] = get_metric_value(metric_name,dev,frg,tbl)
            if values[d,f] > max_v:
                max_v=values[d,f]
            f+=1
            if f>=max_fragments:
                break
        d+=1
        if d>=max_devices:
            break
    return devices[0:n_devices], fragments[0:n_fragments], values, max_v

def get_metric_names():
    metrics = []    
    for r in db(db.t_observed_metric).select(db.t_observed_metric.f_name, distinct=True):
        metrics.append(r.f_name)
    return metrics

def prepare_trainset(metric_name):
    d1 = []
    for r in db(db.t_observed_metric.f_name == metric_name).select():
        d1.append((r.f_device, r.f_fragment, r.f_value ))
    
    train_df = pd.DataFrame(d1, columns=('DeviceID', 'Fragment', 'MetricValue')) 
    #print train_df
    maxv = train_df['MetricValue'].max()
    minv = train_df['MetricValue'].min()
    rd = Reader(rating_scale=(minv, maxv))
    train_data = Dataset.load_from_df(train_df[['DeviceID', 'Fragment', 'MetricValue']], rd)     
    return train_data

def prepare_and_run_svd(metric_name,n_factors=2,n_epochs=20,reg_all=0.002,lr_all=0.005):
    start = time.time()
    train_data = prepare_trainset(metric_name)
    trainset   = train_data.build_full_trainset()
    
    #algo = SVD(n_factors=2,n_epochs=20,reg_all=0.002) #reg_all – The regularization term. Default is 0.02
    algo = SVD(n_factors,n_epochs,reg_all,lr_all) #reg_all – The regularization term. Default is 0.02
    algo.fit(trainset)
    
    devices = []
    for r in db(db.t_observed_metric.f_name == metric_name).select(db.t_observed_metric.f_device, distinct=True):
        devices.append(r.f_device)
        
    fragments = []
    for r in db(db.t_observed_metric.f_name == metric_name).select(db.t_observed_metric.f_fragment, distinct=True):
        fragments.append(r.f_fragment)
    
    #inferred = []
    db(db.t_inferred_metric.f_name == metric_name).delete()
    
    for d in devices:
        for f in fragments:                        
            result = algo.predict(d,f,verbose=False)                    
            est = result.est
            #inferred.append((d,f,est))
            db.t_inferred_metric.insert(f_name=metric_name,f_device=d,f_fragment=f,f_value=est)
    #END
    end = time.time()
    duration=end-start
    return locals()

def tune_svd_params(metric_name,factors,epochs,reg_all,lr_all,cv):
    train_data = prepare_trainset(metric_name)
    #https://kerpanic.wordpress.com/2018/03/26/a-gentle-guide-to-recommender-systems-with-surprise/
    param_grid = {'n_factors': [2,3,4,5,6,7,110, 120, 140, 160], 'n_epochs': [20, 50, 100, 150], 
              'lr_all': [0.001, 0.003, 0.005, 0.008],
              'reg_all': [0.08, 0.02, 0.1, 0.15]}
    gs = GridSearchCV(SVD, param_grid, measures=['rmse', 'mae'], cv=3, n_jobs=-1) # -1: all CPUs used.
    # RandomizedSearchCV alternative : As opposed to GridSearchCV, which uses an exhaustive combinatorial 
    # approach, RandomizedSearchCV samples randomly from the parameter space. 
    gs.fit(train_data)
    
    """
    print "choosing best_estimator "
    print "Best params:", gs.best_params #['rmse']
    print "Best score:", gs.best_score #['rmse']    
    #print "Algo rmse best factors =", gs.best_params['rmse']['n_factors']
    algo = None
    n_factors = None
    #Choose as best estimator the one with the best score !!!
    if gs.best_score['rmse'] > gs.best_score['mae']:
        algo = gs.best_estimator['rmse']
        n_factors = gs.best_params['rmse']['n_factors']
        print "best estimator is rmse"
    else:
        algo = gs.best_estimator['mae']
        n_factors = gs.best_params['mae']['n_factors']
        print "best estimator is mae"
    """    
    return gs

def generate_observed_metrics(metric_name="task_time", 
                              n_features = 2, n_dt = 6, n_devices = 10, n_fragments = 5,
                              max_noise=1.5,ipv6=False,fnames=False):
    N_DT       = int(n_dt)  #device types
    N_FEATURES = int(n_features)  #model features (ram,cpu,io,...)
    N_FRAGS    = int(n_fragments)  #fragments (task types) 
    #N_FACTORS  = 2  # for SVD (if not used it is computed automatically)
    N_DEVICES  = int(n_devices) # you must enter N_DEVICES <= 2*N_DT 
    #N_LOOPS    = 10 # number of control layer simulation steps (loops)
    #N_EXCLUDE  = 3  # number of worst devices to exclude (control layer scheduling algo)    
    
    #task_times = ctrl_execute_tasks(task_times_final,n_devices=N_DEVICES,n_exclude=5)
    
    MAX_NEW_INSTANCES=3 #
    #n_devices = N_DEVICES
    #n_exclude= N_EXCLUDE
    n_instances=1

    np.random.seed(543212345)
    devices_types = np.random.rand(N_DT,N_FEATURES)
    fragments = np.random.randint(0,100,size=(N_FRAGS,N_FEATURES))
    model_task_times = np.dot(devices_types,fragments.T)
    
    task_times_final = np.zeros((N_DEVICES,N_FRAGS)) #gen for N devices TRUE task times
    for i in range(N_DEVICES):
        task_times_final[i] = model_task_times[i/2] #generate two devices per device type
    
    ### DO INITAL PLACMENT (one benchmark per device, one device per task)
    fragments = np.arange(N_FRAGS)
    np.random.shuffle(fragments)

    f = 0
    task_times = np.copy(task_times_final)
    for i in range(N_DEVICES):
        if i % 2 == 0 :
            task_times[i,1:N_FRAGS] = np.full(N_FRAGS-1,-1.0) #[-1.0,-1.0,-1.0,-1.0] #set -1.0 to erase fragments
            noise = np.random.uniform(0,max_noise)
            if f<len(fragments):
                k = fragments[f]
                if k == 0:
                    task_times[i,k] = task_times_final[i,k] # no noise for benchmark fragment
                else:
                    task_times[i,k] = task_times_final[i,k] + noise
                f+=1
        else:
            task_times[i,1:N_FRAGS] = np.full(N_FRAGS-1,-1.0) #set -1.0 to erase fragments
    
    db(db.t_observed_metric.f_name == metric_name).delete()
    output_data = []
    
    #generate random IPv6 for device ids
    device_ipv6 = []
    if ipv6:
        for d in range(N_DEVICES):
            M = 16**4
            ipv6 = "fdff:" + ":".join(("%x" % random.randint(17, M) for i in range(7)))
            device_ipv6.append(ipv6)
    
    #generate random device names
    fragnames = [ "test_fragments_Benchmark",
                  "test_fragments_TestFragment",
                  "test_fragments_MultimediaManager",
                  "test_fragments_VideoTranscoder",
                  "test_fragments_FaceDetector_detection_algorithm",
                  "test_fragments_AudioCaptor",
                  "test_fragments_FaceDetector",
                  "test_fragments_PercussionDetector",
                  "test_fragments_MultimediaManager_intensive_multimedia_processor",
                  "test_fragments_VideoStreamer" ]        

    for d in range(N_DEVICES):
        for f in range(N_FRAGS):
            t = task_times[d,f]
            if t>0:
                #output_data.append("%s,%s,%s\n"%(d,f,t))
                fname = "test_fragments_Fragment" + str(f)
                if f<10:
                    fname = fragnames[f]
                if ipv6:
                    p_f_device   =   "%s"%(device_ipv6[d])                
                else:  
                    p_f_device   =   "%s"%(d)
                if fnames:
                    p_f_fragment = "%s"%(fname)
                else:
                    p_f_fragment ="%s"%(f)
                
                db.t_observed_metric.insert(
                    f_name=metric_name,
                    f_device=p_f_device,
                    f_fragment=p_f_fragment,
                    #f_device="%s"%(device_ipv6[d]),
                    #f_fragment="%s"%fname,
                    #f_device="%s"%(d),
                    #f_fragment="%s"%(f),
                    f_value=t)
    return locals()
