from gluon.contrib.populate import populate

if db(db.auth_user).isempty():
     populate(db.auth_user,10)
     populate(db.t_inferred_metric,10)
     #populate(db.t_observed_metric,10)
