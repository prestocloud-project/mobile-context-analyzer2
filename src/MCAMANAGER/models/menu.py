response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
response.menu = [
(T('Home'),URL('default','index')==URL(),URL('default','index'),[]),
(T('Input'),URL('default','observed_metric_manage')==URL(),URL('default','observed_metric_manage'),[]),
(T('Output'),URL('default','inferred_metric_manage')==URL(),URL('default','inferred_metric_manage'),[]),
(T('Inference'),URL('default','process_data')==URL(),URL('default','process_data'),[]),    
    (T('Tuning'),False,False,[
            (T('SVD Auto-Tuning'),URL('default','svd_param_search')==URL(),URL('default','svd_param_search'),[]),
            (T('Manage Tuning Results'),URL('default','tuning_results_manage')==URL(),URL('default','tuning_results_manage'),[]),
        ]
    ),
(T('Visualization'),URL('visualization','index')==URL(),URL('visualization','index'),[]),    
    (T('Help'),False,False,[
            (T('About'),URL('help','about')==URL(),URL('help','about'),[]),
            (T('API')  ,URL('help','api')==URL(),  URL('help','api'),[]),
            (T('SVD')  ,URL('help','SVD')==URL(),  URL('help','SVD'),[]),
        ]
    ),    
]
