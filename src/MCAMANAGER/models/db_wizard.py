########################################
db.define_table('t_inferred_metric',
    Field('f_name', type='string',
          notnull=True,
          label=T('Name')),
    Field('f_device', type='string',
          notnull=True,
          label=T('Device ID')),
    Field('f_fragment', type='string',
          notnull=True,
          label=T('Fragment ID')),
    Field('f_value', type='double',
          notnull=True,
          label=T('Value')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

#db.define_table('t_inferred_metric_archive',db.t_inferred_metric,Field('current_record','reference t_inferred_metric',readable=False,writable=False))

########################################
db.define_table('t_observed_metric',
    Field('f_name', type='string', 
          notnull=True,
          label=T('Name')),
    Field('f_device', type='string',
          notnull=True,
          label=T('Device ID')),
    Field('f_fragment', type='string',
          notnull=True,
          label=T('Fragment ID')),
    Field('f_value', type='double',
          notnull=True,
          label=T('Value')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

#db.define_table('t_observed_metric_archive',db.t_observed_metric,Field('current_record','reference t_observed_metric',readable=False,writable=False))


########################################
db.define_table('t_tuning_results',
    Field('f_metric_name', type='string', 
          notnull=True,
          label=T('Metric Name')),
    Field('f_estimator', type='string',
          notnull=True,
          label=T('Estimator')),
    Field('f_epochs', type='integer',
          notnull=True,
          label=T('Epochs')),
    Field('f_factors', type='integer',
          notnull=True,
          label=T('Factors')),
    Field('f_reg_all', type='double',
          notnull=True,
          label=T('Regularization Term')),
    Field('f_lr_all', type='double',
          notnull=True,
          label=T('Learning Rate')),                            
    #Field('f_dt', type='datetime',
    #      notnull=True, 
    #      label=T('Created')),            
    auth.signature,
    format='%(f_metric_name)s',
    migrate=settings.migrate)
