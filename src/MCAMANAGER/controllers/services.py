# -*- coding: utf-8 -*-
from gluon.tools import Service
service = Service()
from gluon.contrib.login_methods.basic_auth import basic_auth
#auth.settings.login_methods.append(basic_auth('https://basic.example.com'))
auth.settings.login_methods.append(basic_auth(server="http://127.0.0.1"))
auth.settings.allow_basic_login = True

def index(): return dict(message="hello from services.py")

@auth.requires_login()
def call():
    session.forget()
    return service()

@service.run
@service.json
@service.xml
def executeSVD(metric_name,n_factors=2,n_epochs=20,reg_all=0.002): 
    # http://127.0.0.1:8000/MCAMANAGER/services/call/json/executeSVD?metric_name=latency
    #curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/executeSVD?metric_name=latency
    ret = prepare_and_run_svd(metric_name,n_factors=2,n_epochs=20,reg_all=0.002)
    fragments=ret["trainset"].n_items
    devices=ret["trainset"].n_users
    ratings=ret["trainset"].n_ratings
    
    return dict(metric_name=metric_name,duration=ret["duration"],
                n_fragments=fragments,n_devices=devices,n_ratings=ratings)

@service.run
@service.json
@service.xml
def putObsMetricValue(metric_name,device_id,fragment_id,value): #ONE    
    #result = record_id when insert OR null when update
    #curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/putObsMetricValue/latency/0/0/99
    tbl = db.t_observed_metric
    device_id = device_id.replace("_",":") #for IPV6 literals
    q = (tbl.f_name == metric_name) & ( tbl.f_fragment == fragment_id ) & ( tbl.f_device == device_id )
    r = tbl.update_or_insert(q,  f_name = metric_name, 
                             f_device = device_id, 
                             f_fragment = fragment_id, 
                             f_value = value)
    return dict(value=value, result=r) 

@service.run
@service.json
@service.xml
def putAvgObsMetricValue(metric_name,device_id,fragment_id,value,weight=.5): #ONE    
    #result = record_id when insert OR null when update
    #curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/putAvgObsMetricValue/latency/0/0/50/.1
    new_value=float(value)
    old_value=None
    tbl = db.t_observed_metric
    q = (tbl.f_name == metric_name) & ( tbl.f_fragment == fragment_id ) & ( tbl.f_device == device_id )
    #
    for old in db(q).iterselect(tbl.f_value):
        old_value=float(old.f_value)
        fw = float(weight)
        new_value=float(value)*fw+old_value*(1.0-fw)
    #
    r = tbl.update_or_insert(q,  f_name = metric_name, 
                             f_device = device_id, 
                             f_fragment = fragment_id, 
                             f_value = new_value)
    return dict(old_value=old_value, new_value=new_value, result=r)

@service.run
@service.json
@service.xml
def addObsMetricValues(): #BATCH
    status="SUCCESS"
    recs=150
    return locals()

@service.run
@service.json
@service.xml
def getObsMetricValue(metric_name,device_id=0,fragment_id=0): #ONE
    # curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/getInfMetricValue/latency/1/2
    #tbl = db.t_observed_metric
    tbl=db.t_observed_metric
    v = get_metric_value(metric_name,device_id,fragment_id,tbl)
    return dict(val=v,device_id=device_id,fragment_id=fragment_id)

@service.run
@service.json
@service.xml
def getObsMetricValues(metric_name): #BATCH    
    tbl=db.t_observed_metric
    q = (tbl.f_name==metric_name) 
    data = []
    for r in db(q).iterselect(tbl.f_value,tbl.f_fragment,tbl.f_device):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
    return dict(metric_name=metric_name,data=data)

@service.run
@service.json
@service.xml
def getObsMetricValuesPerDevice(metric_name,device_id): 
    tbl=db.t_observed_metric
    q = (tbl.f_name==metric_name) & (tbl.f_device==device_id)
    data = []
    for r in db(q).select(tbl.f_value,tbl.f_fragment,tbl.f_device):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
    return dict(metric_name=metric_name,data=data)

@service.run
@service.json
@service.xml
def getObsMetricValuesPerFragment(metric_name,fragment_id): 
    tbl=db.t_observed_metric
    q = (tbl.f_name==metric_name) & (tbl.f_fragment==fragment_id)
    data = []
    for r in db(q).select(tbl.f_value,tbl.f_fragment,tbl.f_device):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
    return dict(metric_name=metric_name,data=data)


########## INFERRED
@service.run
@service.json
@service.xml
def getInfMetricValue(metric_name,device_id=0,fragment_id=0): #ONE
    # curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/getInfMetricValue/latency/1/2
    #tbl = db.t_observed_metric
    tbl=db.t_inferred_metric
    v = get_metric_value(metric_name,device_id,fragment_id,tbl)
    return dict(val=v,device_id=device_id,fragment_id=fragment_id)

@service.run
@service.json
@service.xml
def getInfMetricValues(metric_name): #BATCH    
    tbl=db.t_inferred_metric
    q = (tbl.f_name==metric_name) 
    data = []
    for r in db(q).iterselect(tbl.f_value,tbl.f_fragment,tbl.f_device):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
    return dict(metric_name=metric_name,data=data)

@service.run
@service.json
@service.xml
def getInfMetricValuesPerDevice(metric_name,device_id): 
    tbl=db.t_inferred_metric
    q = (tbl.f_name==metric_name) & (tbl.f_device==device_id)
    data = []
    for r in db(q).select(tbl.f_value,tbl.f_fragment,tbl.f_device):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
    return dict(metric_name=metric_name,data=data)

@service.run
@service.json
@service.xml
def getInfMetricValuesPerFragment(metric_name,fragment_id): 
    tbl=db.t_inferred_metric
    q = (tbl.f_name==metric_name) & (tbl.f_fragment==fragment_id)
    data = []
    for r in db(q).select(tbl.f_value,tbl.f_fragment,tbl.f_device):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
    return dict(metric_name=metric_name,data=data)

@service.run
@service.json
@service.xml
def getPercentTopDevices(metric_name,fragment_id,pct=70,order="DESC"):    
    # curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/getPercentTopDevices/latency/0/25/ASC
    tbl=db.t_inferred_metric
    if order=="DESC":
        orderby=~tbl.f_value
    else:
        orderby=tbl.f_value
    q = (tbl.f_name==metric_name) & (tbl.f_fragment==fragment_id)
    cnt = db(q).count()
    ret_max = int(int(pct)*cnt/100)
    data = []
    i = 0
    for r in db(q).iterselect(tbl.f_value,tbl.f_fragment,tbl.f_device,orderby=orderby):
        data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
        i+=1
        if i>ret_max:
            break
    return dict(metric_name=metric_name,data=data,total_records=cnt,top_records=ret_max,pct=pct)

@service.run
@service.json
@service.xml
def getExcludedDevicesPerMetric(metric_name,pct=30,worst="LOW"):    
    #pct : percent of devices to exclude , 
    #order="DESC" : exclude the devices with lower metric
    # curl --user admin@presto.eu:admin http://127.0.0.1:8000/MCAMANAGER/services/call/json/getExcludedDevices/task_time/25
    tbl=db.t_inferred_metric
    
    if worst=="LOW":
        orderby=~tbl.f_value
    else:
        orderby=tbl.f_value
    
    m_data = []
    q_f = (tbl.f_name==metric_name) #iterate all fragments
    for r_f in db(q_f).iterselect(tbl.f_fragment, distinct=True, orderby=tbl.f_fragment):
        fragment_id=r_f.f_fragment
        q = (tbl.f_name==metric_name) & (tbl.f_fragment==fragment_id)
        cnt = db(q).count()
        ret_max = int(int(pct)*cnt/100)
        i = 0
        data = []
        for r in db(q).iterselect(tbl.f_value,tbl.f_fragment,tbl.f_device,orderby=orderby):
            #data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
            #data.append(dict(fragment_id=r.f_fragment,device_id=r.f_device,value=r.f_value))
            data.append(r.f_device)
            i+=1
            if i>ret_max:
                break
        m_data.append(dict(fragment_name=r.f_fragment,excluded_devices=data))
        #m_data.append(dict(excluded_devices=data,fragment_name=r.f_fragment))
    return dict(data=m_data)
