# -*- coding: utf-8 -*-
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
plt.ioff()

@auth.requires_login()
def index():
    def_max_devices = session.max_devices
    def_max_fragments = session.max_fragments
    if def_max_devices is None: def_max_devices = 30
    if def_max_fragments is None: def_max_fragments = 30
    form = SQLFORM.factory(
        Field('name',label="Metric Name",
              requires=IS_IN_DB(db(db.t_inferred_metric),
              't_inferred_metric.f_name',
              '%(f_name)s', zero=T('choose one'), distinct=True)),
        Field('max_devices',label="Maximum Number of Devices",
              default=def_max_devices, type="integer"),
        Field('max_fragments',label="Maximum Number of Fragments",
              default=def_max_fragments, type="integer"),
        Field('font_scale',label="Font Scale",
              default=0.8, type="float"),
        Field('annot',type="boolean",label="Show Metric Values", default=True),
    )
    submit = form.element("input",_type="submit")
    submit["_value"] = "Compare"
    if form.process().accepted:
        metric_name=request.vars.name
        max_devices=request.vars.max_devices
        max_fragments=request.vars.max_fragments
        font_scale = request.vars.font_scale
        #session.flash = 'SVD executed for metric : ' + metric_name
        #redirect(URL('index', args=(1, 2, 3), vars=dict(a='b')))
        session.metric_name = metric_name
        session.max_devices = int(max_devices)
        session.max_fragments = int(max_fragments)
        session.font_scale = font_scale
        session.heatmap_annot = request.vars.annot
        redirect(URL('compare'))
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill the form'
    return dict(form=form)

@auth.requires_login()
def compare():
    metric_name = session.metric_name
    return locals()

@auth.requires_login()
def heatmap(metric_name,tbl,font_scale=1.0):
    import cStringIO
    stream=cStringIO.StringIO()
    sns.set()
    sns.set_style("dark")
    sns.set(font_scale=font_scale)
    
    p_annot = False #display heatmap values in the cells
    if session.heatmap_annot == 'on':
        p_annot = True
    
    db_tbl = db.t_observed_metric
    if tbl=="inf":
        db_tbl = db.t_inferred_metric
    #   
    yticklabels, xticklabels, metric_values, vmax = get_metric_data_arr(
        metric_name,db_tbl,session.max_devices ,session.max_fragments)
    ###
    #    
    ax = sns.heatmap(metric_values, vmin=0.0,vmax=vmax, annot=p_annot, fmt='2.2f', cmap="YlGnBu",
                     yticklabels=yticklabels,
                     linewidths=.5, mask=metric_values < 0  ) #, cbar = 0 )
    ###
    ax.xaxis.set_ticks_position('top')
    #ax.set_xticklabels(xticklabels, rotation=60, ha='left')
    ax.set_xticklabels(xticklabels, rotation=25, ha='left')
    ax.set_title("Fragment ID")

    ax.title.set_position([0.5,-.1])
    ax.set(ylabel='Device ID')
    ###
    fig = ax.get_figure()
    fig.savefig(stream, format='png',bbox_inches='tight')
    plt.clf() #clear axes (and colorbars)
    return stream

@auth.requires_login()
def obs_heatmap():
    response.headers['Content-Type']='image/png'
    metric_name = session.metric_name
    font_scale = session.font_scale
    stream = heatmap(metric_name,"obs",font_scale=float(font_scale))
    return stream.getvalue()

@auth.requires_login()
def inf_heatmap():
    response.headers['Content-Type']='image/png'
    metric_name = session.metric_name
    font_scale = session.font_scale
    stream = heatmap(metric_name,"inf",font_scale=float(font_scale))
    return stream.getvalue()

@auth.requires_login()
def test():
    response.headers['Content-Type']='image/png'
    import cStringIO
    stream=cStringIO.StringIO()
    sns.set_style("dark")
    y = [1,2,5,6,2]
    x = [0,1,2,3,4]
    plt.plot(x,y)
    plt.savefig(stream, format='png')
    ##img.seek(0)
    return stream.getvalue()
