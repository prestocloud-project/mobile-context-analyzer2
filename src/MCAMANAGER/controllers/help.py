# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from help.py")

def about(): return dict(message="about")

@auth.requires_login()
def api(): return dict(message="api")

def SVD(): return dict(message="SVD")
