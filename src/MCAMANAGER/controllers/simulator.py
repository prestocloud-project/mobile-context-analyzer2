# -*- coding: utf-8 -*-
# try something like
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import random
import pandas as pd
import seaborn as sns

from surprise import *
from surprise import accuracy
from surprise.model_selection import train_test_split

from surprise.model_selection import cross_validate
from surprise.model_selection import GridSearchCV

from IPython.display import display_html
from IPython.core.display import HTML

np.set_printoptions(precision=2) #print with 2 decimals
pd.options.mode.use_inf_as_na = True
sns.set()


def index(): return dict(message="hello from simulator.py")

def gen_observed_metrics():
    metric_name = "reponse_time"    
    form = SQLFORM.factory(
        Field('name',type="string",label="Metric Name", requires = IS_NOT_EMPTY()),
        Field('dt',type="integer",label="Device Types", default=5, requires = IS_INT_IN_RANGE(2, 10000000)),
        Field('dev_p_dt',type="integer",label="Devices per Type", default=2, requires = IS_INT_IN_RANGE(2, 1000)),
        Field('frags',type="integer",label="Fragments", default=5, requires = IS_INT_IN_RANGE(2, 10000000)),
        Field('max_noise',type="double",label="Max Noise", default=1.5),
        Field('features',type="integer",label="Features", default=2, requires = IS_INT_IN_RANGE(2, 100)),
        Field('ipv6',type="boolean",label="Generate IPV6 Device IDs", default=True),
        Field('fnames',type="boolean",label="Use Predefined Test Fragment Names", default=True),
    )
    ###
    submit = form.element("input",_type="submit")
    submit["_value"] = "Generate Observed Metrics"
    ###
    if form.process().accepted:        
        metric_name=request.vars.name
        ipv6=request.vars.ipv6
        fnames=request.vars.fnames
        features=request.vars.features
        dt=int(request.vars.dt)
        dev_p_dt=int(request.vars.dev_p_dt)
        frags=int(request.vars.frags)
        max_noise=float(request.vars.max_noise)
        #n_features = 2, n_dt = 6, n_devices = 10, n_fragments = 5,
        generate_observed_metrics(metric_name,
                                  max_noise=max_noise,
                                  ipv6=ipv6,
                                  fnames=fnames,
                                  n_features=features,
                                  n_dt = dt,
                                  n_devices = dt * dev_p_dt,
                                  n_fragments = frags
                                 ) #call business_logic in models
        session.flash = 'SVD executed for metric : ' + metric_name
        #redirect(URL('index', args=(1, 2, 3), vars=dict(a='b')))
        session.metric_name = metric_name        
        redirect(URL('default','observed_metric_manage'))
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill the form'
    return dict(form=form)
