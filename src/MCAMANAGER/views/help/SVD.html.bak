{{extend 'layout.html'}}
<!-- Loading mathjax macro -->
<!-- Load mathjax -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_HTML"></script>
    <!-- MathJax configuration -->
    <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [ ['$','$'], ["\\(","\\)"] ],
            displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
            processEscapes: true,
            processEnvironments: true
        },
        // Center justify equations in code and markdown cells. Elsewhere
        // we use CSS to left justify single line equations in code cells.
        displayAlign: 'center',
        "HTML-CSS": {
            styles: {'.MathJax_Display': {"margin": 0}},
            linebreaks: { automatic: true }
        }
    });
    </script>
    <!-- End of mathjax configuration --></head>



<h1>SVD Algorithm used in Mobile Context Analyzer</h1>


<H6>This SVD algorithm was used by Simon Funk during the Netflix Prize. </h6>
<br/>
<p>
The expected metric value $\hat{m}_{dm}$ is computed with the equation :    
</p>

\begin{equation*}
\hat{m}_{df} = \mu + b_d + b_f + q_f^Tp_d
\end{equation*}

Where :  
<li> $d$ : device index  </li>
<li> $f$ : fragment index </li>
<li> $m$ : metric value </li> 
<li> $b_d$ : device bias </li>
<li> $b_f$ : fragment bias </li>
<li> $q_f$ : fragment latent factors </li>
<li> $p_d$ : device latent factors </li>

<br/>

<p>
    
To estimate all the unknown parameters ($ \mu, b_d, b_f , q_f , p_d $), we minimize the following regularized squared error:
</p>

\begin{equation*}
\sum_{m_{df} \in M_{train}} \left(m_{df} - \hat{m}_{df} \right)^2 +
\lambda\left(b_f^2 + b_d^2 + ||q_f||^2 + ||p_d||^2\right)
\end{equation*}

<p>
The minimimum of the above expression can be computed with the methods of stochastic gradient descent (SGD) or alternated least square errors (ALS).     
</p>

MCA is implemented with the python <a href="http://surpriselib.com/">Suprise</a> library which uses SGD.
