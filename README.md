[![logo](http://prestocloud-project.eu/wp-content/uploads/2018/06/logo.png)](http://prestocloud-project.eu)
## Mobile Context Analyzer 

Overview
------------
Mobile Context Analyzer v2 is a system for the context-aware prediction of edge-device QoS Metrics.
A complete desciption of the system is provided in the PrEstocloud deliverable D3.6

Installation (Ubuntu 18)
------------------------------
The system is developed and tested on Ubuntu 18 Linux.
Please perform the following installation steps : 
 
	$ sudo apt update
Download and install Web2py :

	$ wget https://mdipierro.pythonanywhere.com/example/static/web2py_src.zip
	$ sudo apt install unzip  
	$ unzip web2py_src.zip
	$ cd web2py/scripts/   
	$ sudo bash setup-web2py-ubuntu.sh
Install Python libraries :

	$ sudo apt install python-pandas   
	$ sudo pip install --upgrade pip   
	$ sudo pip install scikit-surprise
	$ sudo mkdir /var/www/.surprise_data
	$ sudo chmod a+w /var/www/.surprise_data
	$ sudo pip install seaborn
	$ sudo pip install ipython
Edit matplotlib configuration file :
	
	$ sudo vi /etc/matplotlibrc
Set Agg as matplotlib backend : 

    #backend      : TkAgg
    backend      : Agg
Save and close the file matplotlibrc (Esc wq).   
Restart Apache :

	$ sudo service apache2 restart


Login in web2py admin interface 
and install the MCAMANAGER application from the file : bin/web2py.app.MCAMANAGER.w2p

Usage
---------------
MCA provides a web-based interface.
The Web-based interface of MCA is described in  D3.6 and [doc/WebInterface.docx link to word document ]
 
The PrEstocloud Mobile Context Analyzer (MCA) uses application fragment Qos Metrics. 
These metrics can be inserted into the MCA database either via the web interface or via the API. 

An adapter is provided for this purpose in the directory src/MCAadapters/MCA_Monitoring_Adapter.py. 
Please read in the corresponding directory the installation and usage instructions for this adapter.  

The resutls of MCA (the predicted QoS Metrics) are queried and used by the PrEstoCloud DIADDRecom component.
An adapter is provided for this purpose in the directory src/MCAadapters/MCA_DIAFDRecom_Adapter.py.
Please read in the corresponding directory the installation and usage instructions for this adapter.

   
